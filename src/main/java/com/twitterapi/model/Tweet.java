package com.twitterapi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "tweets")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Tweet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 100)
	@NotBlank
	private String usuario;
	
	@Column(length = 300)
	@NotBlank
	private String texto;
	
	@Column(length = 200)
	private String localizacion;
	
	private Boolean validado;
}
