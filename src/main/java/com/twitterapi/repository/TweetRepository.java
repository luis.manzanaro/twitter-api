package com.twitterapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.twitterapi.model.Tweet;

public interface TweetRepository extends JpaRepository<Tweet, Long>{
	
	public List<Tweet> findByValidadoTrue();

}
