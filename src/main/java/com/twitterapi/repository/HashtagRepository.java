package com.twitterapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.twitterapi.model.Hashtag;

public interface HashtagRepository extends JpaRepository<Hashtag, Long>{
	
	public Optional<Hashtag> findByHashtag(String hashtag);

}
