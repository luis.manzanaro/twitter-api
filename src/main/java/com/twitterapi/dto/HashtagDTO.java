package com.twitterapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HashtagDTO {
	
	private Long id;
	private String hashtag;
	private Long repeticiones;
	
}
