package com.twitterapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TweetDTO {
	
	private Long id;
	private String usuario;
	private String texto;
	private String localizacion;
	private Boolean validado;

}
