package com.twitterapi.configuration;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.twitterapi.constants.Constants;
import com.twitterapi.model.Hashtag;
import com.twitterapi.model.Tweet;
import com.twitterapi.service.HashtagService;
import com.twitterapi.service.TweetService;
import com.twitterapi.service.TwitterService;

import twitter4j.HashtagEntity;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

@Configuration
public class TwitterConfiguration {
	
	@Autowired
	private TweetService tweetService;
	
	@Autowired
	private HashtagService hashtagService;
	
	@Bean
	public TwitterStream twitterStream() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setDebugEnabled(true)
		.setOAuthConsumerKey("xxxxxxx")
		.setOAuthConsumerSecret("xxxxxxx")
		.setOAuthAccessToken("xxxxxxx")
		.setOAuthAccessTokenSecret("xxxxxxx");
		
		return new TwitterStreamFactory(configurationBuilder.build()).getInstance();
	}
	
	
	@Bean
	public StatusListener statusListener() {
		
		StatusListener statusListener = new StatusListener() {
			@Override
	        public void onStatus(Status status) {
	            
	            if (status.getUser().getFollowersCount() > Constants.DEFAULT_NUMBER_FOLLOWERS) {
	            	
	            	User user = status.getUser();
	            	
	            	// Sacamos los datos que vamos a guardar
	                String username = status.getUser().getScreenName();
	                String profileLocation = user.getLocation();
	                String content = status.getText();
	                HashtagEntity hashTags[] = status.getHashtagEntities();
	                
	                System.out.println("UserName: " + username);
	                System.out.println("UserLocation: " + profileLocation);
	                System.out.println("Tweet: " + content);
	                
	                Tweet tweet = Tweet.builder()
	                				.usuario("@"+username)
	                				.texto(content)
	                				.localizacion(profileLocation)
	                				.build();
	                
	                tweetService.saveTweet(tweet);
	                
	                HashSet<String> hashTagContainer = new HashSet<String>();
	                for(HashtagEntity hashTag: hashTags) {
	                  hashTagContainer.add(hashTag.getText());
	                  
	                  hashtagService.saveHashtag(hashTag.getText());
	                }
	                
	                System.out.println("HashTags: " + hashTagContainer);
	                System.out.println(status.getLang());
	            }
	        }

	        @Override
	        public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
	            
	        }

	        @Override
	        public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
	            System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
	        }

	        @Override
	        public void onScrubGeo(long userId, long upToStatusId) {
	            System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
	        }

	        @Override
	        public void onStallWarning(StallWarning warning) {
	            System.out.println("Got stall warning:" + warning);
	        }

	        @Override
	        public void onException(Exception ex) {
	            ex.printStackTrace();
	        }
		};
		
		return statusListener;
		
	}
	
	

}
