package com.twitterapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.twitterapi.dto.MessageDTO;

@ControllerAdvice
public class ResponseExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<MessageDTO> exceptionHandler(Exception e){
		
		MessageDTO exception =  MessageDTO.builder()
											.codigo(500)
											.mensaje(e.getMessage())
											.build();
		
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception);
		
		
	}
	
	
	@ExceptionHandler(ObjectNotFoundException.class)
	public ResponseEntity<MessageDTO> objectNotFoundExceptionHandler(ObjectNotFoundException e){
		MessageDTO exception =  MessageDTO.builder()
				.codigo(404)
				.mensaje(e.getMessage())
				.build();
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception);
	}
}
