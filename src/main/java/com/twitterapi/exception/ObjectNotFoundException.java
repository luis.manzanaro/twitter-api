package com.twitterapi.exception;

public class ObjectNotFoundException extends RuntimeException{

	private static final long serialVersionUID = -8582789526577000697L;
	
	public ObjectNotFoundException(String message) {
		super(message);
	}

}
