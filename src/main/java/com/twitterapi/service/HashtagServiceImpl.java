package com.twitterapi.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.twitterapi.constants.Constants;
import com.twitterapi.dto.HashtagDTO;
import com.twitterapi.model.Hashtag;
import com.twitterapi.repository.HashtagRepository;

@Service
public class HashtagServiceImpl implements HashtagService{
	
	@Autowired
	private HashtagRepository hashtagRepository;

	@Override
	public List<HashtagDTO> getHashtags(Optional<Integer> limit) {
		Integer pageLimit = (limit.isPresent()) ? limit.get() : Constants.DEFAULT_HASHTAGS;
		Pageable page = PageRequest.of(0, pageLimit, Sort.by("repeticiones").descending());
		
		return hashtagRepository.findAll(page).getContent().stream()
												.map(h -> {
													HashtagDTO hashtag = HashtagDTO.builder()
																			.id(h.getId())
																			.hashtag(h.getHashtag())
																			.repeticiones(h.getRepeticiones())
																			.build();
													return hashtag;
												}).collect(Collectors.toList());
	}

	@Override
	public void saveHashtag(String hashtag) {
		Optional<Hashtag> hashtagObject = hashtagRepository.findByHashtag(hashtag.toUpperCase().trim());
		
		if (hashtagObject.isPresent()) {
			Long repeticiones = hashtagObject.get().getRepeticiones();
			hashtagObject.get().setRepeticiones(repeticiones + 1);
			
			hashtagRepository.save(hashtagObject.get());
		}
		else {
			Hashtag newHashtag = Hashtag.builder()
									.hashtag(hashtag.toUpperCase().trim())
									.repeticiones(1L)
									.build();
			
			hashtagRepository.save(newHashtag);
		}
		
	}

	
}
