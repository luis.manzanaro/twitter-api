package com.twitterapi.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twitterapi.dto.TweetDTO;
import com.twitterapi.exception.ObjectNotFoundException;
import com.twitterapi.model.Tweet;
import com.twitterapi.repository.TweetRepository;

@Service
public class TweetServiceImpl implements TweetService{
	
	@Autowired
	private TweetRepository tweetRepository;

	@Override
	public void saveTweet(Tweet tweet) {
		
		tweetRepository.save(tweet);
		
	}

	@Override
	public List<TweetDTO> getTweets() {
		
		return tweetRepository.findAll().stream()
										.map(tw -> {
											TweetDTO tweet = TweetDTO.builder()
																	.id(tw.getId())
																	.usuario(tw.getUsuario())
																	.texto(tw.getTexto())
																	.localizacion(tw.getLocalizacion())
																	.build();
											return tweet;
										}).collect(Collectors.toList());
										
		
	}

	@Override
	public List<TweetDTO> getValidatedTweets() {
		
		return tweetRepository.findByValidadoTrue().stream()
				.map(tw -> {
					TweetDTO tweet = TweetDTO.builder()
											.id(tw.getId())
											.usuario(tw.getUsuario())
											.texto(tw.getTexto())
											.localizacion(tw.getLocalizacion())
											.build();
					return tweet;
				}).collect(Collectors.toList());
	}
	
	
	@Override
	public void validateTweet(Long id) {
		Optional<Tweet> tweet = tweetRepository.findById(id);
		
		if (!tweet.isPresent()) {
			throw new ObjectNotFoundException("No se ha encontrado ningun tweet para validar");
		}
		
		tweet.get().setValidado(true);
		tweetRepository.save(tweet.get());
	}

}
