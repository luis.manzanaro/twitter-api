package com.twitterapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.twitterapi.constants.Constants;

import twitter4j.FilterQuery;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;

@Service
public class TwitterServiceImpl implements TwitterService{
	
	@Autowired
	private TwitterStream ts;
	
	@Autowired
	private StatusListener sl;

	@Override
	public void readData() {
		ts.addListener(sl);
		FilterQuery query = new FilterQuery();
		query.track("web");
		query.language(Constants.DEFAULT_TWEETS_LANGUAGE);
		ts.filter(query);
	}
	
	
	
}


