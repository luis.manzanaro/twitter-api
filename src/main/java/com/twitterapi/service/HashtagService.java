package com.twitterapi.service;

import java.util.List;
import java.util.Optional;

import com.twitterapi.dto.HashtagDTO;

public interface HashtagService {
	
	public List<HashtagDTO> getHashtags(Optional<Integer> Limit);
	
	public void saveHashtag(String hashtag);

}
