package com.twitterapi.service;

import java.util.List;

import com.twitterapi.dto.TweetDTO;
import com.twitterapi.model.Tweet;

public interface TweetService {
	
	public void saveTweet(Tweet tweet);
	
	public List<TweetDTO> getTweets();
	
	public List<TweetDTO> getValidatedTweets();
	
	public void validateTweet(Long id);
	
}
