package com.twitterapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.twitterapi.service.TwitterService;

@SpringBootApplication
public class TwitterApiApplication implements CommandLineRunner{
	
	@Autowired
	private TwitterService twitterService;

	public static void main(String[] args) {
		SpringApplication.run(TwitterApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		twitterService.readData();
	}

}
