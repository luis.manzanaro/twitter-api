package com.twitterapi.constants;

public class Constants {
	
	public static final Integer DEFAULT_HASHTAGS = 10;
	public static final Integer DEFAULT_NUMBER_FOLLOWERS = 1500;
	public static final String[] DEFAULT_TWEETS_LANGUAGE = new String[] { "es", "it", "fr" };

}
