package com.twitterapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.twitterapi.dto.HashtagDTO;
import com.twitterapi.service.HashtagService;

@RestController
@RequestMapping(value = "/hashtags")
public class HashtagController {
	
	@Autowired
	private HashtagService hashtagService;

	@GetMapping(value = "/")
	public ResponseEntity<List<HashtagDTO>> getHashtags(@RequestParam(required = false) Optional<Integer> limit){
		return ResponseEntity.ok(hashtagService.getHashtags(limit));
	}
}
