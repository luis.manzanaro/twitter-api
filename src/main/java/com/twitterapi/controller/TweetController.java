package com.twitterapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.twitterapi.dto.MessageDTO;
import com.twitterapi.dto.TweetDTO;
import com.twitterapi.service.TweetService;

@RestController
@RequestMapping(value = "/tweets")
public class TweetController {

	@Autowired
	private TweetService tweetService;
	
	@GetMapping(value = "/")
	public ResponseEntity<List<TweetDTO>> getAllTweets(){
		
		return ResponseEntity.ok(tweetService.getTweets());
		
	}
	
	@GetMapping(value = "/validated")
	public ResponseEntity<List<TweetDTO>> getValidatedTweets(){
		
		return ResponseEntity.ok(tweetService.getValidatedTweets());
		
	}
	
	
	@PutMapping(value = "/validate/{id}")
	public ResponseEntity<Object> validateTweet(@PathVariable Long id){
		
		tweetService.validateTweet(id);
		MessageDTO respuesta = MessageDTO.builder()
								.codigo(200)
								.mensaje("El tweet ha sido validado")
								.build();
		
		return ResponseEntity.ok(respuesta);
		
	}
	
}
